package model;

import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class FileManager {
    // Attribute(s)

    // Constructor(s)

    // Getter(s)

    // Setter(s)

    // Other(s)
    public String getFileExtension(String filename) {
        return FilenameUtils.getExtension(filename);
    }

    public File openFileChooser() throws IOException {
        File tempFile = null;

        // Java Swing – JFileChooser example
        // source: https://mkyong.com/swing/java-swing-jfilechooser-example
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select a directory to scan");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        // sources:
        // https://stackoverflow.com/questions/13516829/jfilechooser-change-default-directory-in-windows
        // https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html
        // NOTE
        // For some reasons, the 'setCurrentDirectory()' throw an exception with OpenJDK15...
        // Exception in thread "main" java.lang.IndexOutOfBoundsException: Invalid index
        // fc.setCurrentDirectory(new File(System.getProperty("user.home")));

        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            tempFile = fc.getSelectedFile();
            System.out.println("Opening file: " + tempFile.getAbsolutePath());
        } else {
            System.out.println("Open command cancelled by user.");
        }

        return tempFile;
    }

    public void listFilesInsideFolder(File folder) {
        if (folder.isFile() && isUnwantedFile(folder)) {
            System.out.println("File found: " + folder.getAbsolutePath());
            if (folder.delete()) {
                System.out.println("File '" + folder.getAbsolutePath() + "' has been deleted.\n");
            }
        } else {
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile() && isUnwantedFile(listOfFile)) {
                        System.out.println("File found: " + listOfFile.getAbsolutePath());
                        if (listOfFile.delete()) {
                            System.out.println("File '" + listOfFile.getAbsolutePath() + "' has been deleted.\n");
                        }
                    } else if (listOfFile.isDirectory()) {
                        listFilesInsideFolder(listOfFile);
                    }
                }
            }
        }
    }

    public boolean isUnwantedFile(File file) {
        String[] fileExtensions = new String[]{"srt", "vtt", "url"};
        for (String fileExtension : fileExtensions) {
            if (FilenameUtils.getExtension(String.valueOf(file)).equalsIgnoreCase(fileExtension)) {
                return true;
            }
        }

        return false;
    }

    public void listZIPFilesInsideFolder(File folder) {
        if (folder.isFile() && getFileExtension(folder.getAbsolutePath()).equalsIgnoreCase("zip")) {
            System.out.println("ZIP File found: " + folder.getAbsolutePath());
        } else {
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile() && getFileExtension(listOfFile.getAbsolutePath()).equalsIgnoreCase("zip")) {
                        System.out.println("ZIP File found: " + listOfFile.getAbsolutePath());
                    } else if (listOfFile.isDirectory()) {
                        listZIPFilesInsideFolder(listOfFile);
                    }
                }
            }
        }
    }

    public void listMSOfficeFilesInsideFolder(File file) {
        if (file.isFile() && isMSOfficeFile(file)) {
            System.out.println("MS OFFICE File found: " + file.getAbsolutePath());
        } else {
            File[] listOfFiles = file.listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile() && isMSOfficeFile(listOfFile)) {
                        System.out.println("MS OFFICE File found: " + listOfFile.getAbsolutePath());
                    } else if (listOfFile.isDirectory()) {
                        listMSOfficeFilesInsideFolder(listOfFile);
                    }
                }
            }
        }
    }

    public boolean isMSOfficeFile(File file) {
        String[] fileExtensions = new String[]{"doc", "docx", "xls", "xlsm", "xlsx", "ppt", "pptx"};
        for (String fileExtension : fileExtensions) {
            if (FilenameUtils.getExtension(String.valueOf(file)).equalsIgnoreCase(fileExtension)) {
                return true;
            }
        }

        return false;
    }

    public void listProgrammingFilesInsideFolder(File file, FileExtensionTracker fileExtensionTracker) {
        if (file.isFile() && isProgrammingFile(file)) {
            System.out.println("PROGRAMMING File found: " + file.getAbsolutePath());
            String temp = getFileExtension(file.getAbsolutePath());
            fileExtensionTracker.add(temp);
        } else {
            File[] listOfFiles = file.listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile() && isProgrammingFile(listOfFile)) {
                        System.out.println("PROGRAMMING File found: " + listOfFile.getAbsolutePath());
                        String temp = getFileExtension(listOfFile.getAbsolutePath());
                        fileExtensionTracker.add(temp);
                    } else if (listOfFile.isDirectory()) {
                        listProgrammingFilesInsideFolder(listOfFile, fileExtensionTracker);
                    }
                }
            }
        }
    }

    public boolean isProgrammingFile(File file) {
        // source: https://www.computerhope.com/issues/ch001789.htm
        String[] fileExtensions = new String[]{
                "asp",
                "aspx",
                "cpp",
                "cs",
                "htm",
                "html",
                "java",
                "class",
                "js",
                "json",
                "jsp",
                "xml",
                "php",
                "py",
                "sh",
                "vb",
                "yaml",
                "yml"
        };
        for (String fileExtension : fileExtensions) {
            if (FilenameUtils.getExtension(String.valueOf(file)).equalsIgnoreCase(fileExtension)) {
                return true;
            }
        }

        return false;
    }
}
