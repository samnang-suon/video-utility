package model;

import java.util.ArrayList;

public class FileExtensionTracker {
    // Attribute(s)
    ArrayList<String> fileExtension;

    // Constructor(s)
    public FileExtensionTracker() {
        this.fileExtension = new ArrayList<>();
    }

    // Getter(s)

    // Setter(s)

    // Other(s)
    public void add(String fileExtension) {
        boolean isDuplicate = false;

        for(int i=0; i <this.fileExtension.size(); i++) {
            if(this.fileExtension.get(i).equalsIgnoreCase(fileExtension)) {
                isDuplicate = true;
                break;
            }
        }

        if(isDuplicate == false) {
            this.fileExtension.add(fileExtension);
        }
    }

    public void printAll() {
        System.out.println("\n\n============================== FILE_EXTENSION_TRACKER ==============================");
        for(int i=0; i < this.fileExtension.size(); i++) {
            System.out.println(">>> " + this.fileExtension.get(i));
        }
    }
}
