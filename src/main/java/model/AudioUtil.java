package model;/*
    Source from: https://www.programmersought.com/article/24293833539/
*/
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;

public class AudioUtil {



    /**
     * Get voice file playing time (seconds) support wav format
     * @param filePath
     * @return
     */
    public static Float getDuration(String filePath){
        try{

            File destFile = new File(filePath);
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(destFile);
            AudioFormat format = audioInputStream.getFormat();
            long audioFileLength = destFile.length();
            int frameSize = format.getFrameSize();
            float frameRate = format.getFrameRate();
            float durationInSeconds = (audioFileLength / (frameSize * frameRate));
            return durationInSeconds;

        }catch (Exception e){
            e.printStackTrace();
            return 0f;
        }

    }

    /**
     * Get mp3 voice file playing time (seconds) mp3
     * @param filePath
     * @return
     */
    public static Float getMp3Duration(String filePath){

        try {
            File mp3File = new File(filePath);
            MP3File f = (MP3File) AudioFileIO.read(mp3File);
            MP3AudioHeader audioHeader = (MP3AudioHeader)f.getAudioHeader();
            return Float.parseFloat(audioHeader.getTrackLength()+"");
        } catch(Exception e) {
            e.printStackTrace();
            return 0f;
        }
    }


    /**
     * Get the playing time of mp3 voice file (seconds)
     * @param mp3File
     * @return
     */
    public static Float getMp3Duration(File mp3File){

        try {
            //File mp3File = new File(filePath);
            MP3File f = (MP3File) AudioFileIO.read(mp3File);
            MP3AudioHeader audioHeader = (MP3AudioHeader)f.getAudioHeader();
            return Float.parseFloat(audioHeader.getTrackLength()+"");
        } catch(Exception e) {
            e.printStackTrace();
            return 0f;
        }
    }


    /**
     * The number of milliseconds to get the pcm file
     *
     * Calculation of audio duration of pcm file
     * As with image bmp files, pcm files store uncompressed audio information. 16bits encoding means that the audio information of each sample is saved with 2 bytes. You can compare the bmp file with 2 bytes to save the RGB color information. The 16000 sampling rate refers to sampling 16000 times per second. The common audio frequency is 44100HZ, which means 44100 samples per second. Mono: Only one channel.
     *
     * Based on this information, we can calculate: The audio file size of 16000 sampling rate in 1 second is 2 * 16000 = 32000 bytes, about 32K The audio file size of 8000 sampling rate in 1 second is 2 * 8000 = 16000 bytes, about 16K
     *
     * If the recording duration is known, you can calculate whether the sampling rate is normal according to the file size.
     * @param filePath
     * @return
     */
    public static long getPCMDurationMilliSecond(String filePath) {
        File file = new File(filePath);

        // How many seconds to get
        long second = file.length() / 32000 ;

        long milliSecond = Math.round((file.length() % 32000)   / 32000.0  * 1000 ) ;

        return second * 1000 + milliSecond;
    }
}