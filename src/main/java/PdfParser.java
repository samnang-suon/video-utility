import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PdfParser extends PDFTextStripper {
    static List<String> lines = new ArrayList<String>();

    /**
     * Instantiate a new PDFTextStripper object.
     *
     * @throws IOException If there is an error loading the properties.
     */
    public PdfParser() throws IOException {
    }

    public static void main(String[] args) {
        try {
            // source: https://mkyong.com/swing/java-swing-jfilechooser-example
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            jfc.setDialogTitle("Open a PDF file: ");
            jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            jfc.setMultiSelectionEnabled(false);

            jfc.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF only", "pdf");
            jfc.addChoosableFileFilter(filter);

            int returnValue = jfc.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = jfc.getSelectedFile();
                System.out.println(selectedFile.getAbsolutePath());

                // source: https://mkyong.com/java/pdfbox-how-to-read-pdf-file-in-java/
                PDDocument document = PDDocument.load(selectedFile);
                System.out.println("Open file '" + selectedFile.getName() + "'");
                System.out.println("Number of pages: " + document.getNumberOfPages());

                System.out.println("==================== OUTPUT CONTENT ====================");
                /*
                PDPageTree pageTree = document.getPages();
                for(int i=0; i < pageTree.getCount(); i++) {
                    InputStream inputStream = pageTree.get(i).getContents();

                    // source: https://www.baeldung.com/convert-input-stream-to-string
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream));
                    int c = 0;
                    while ((c = reader.read()) != -1) {
                        System.out.println((char) c);
                    }
                }
                */

                // source: https://mkyong.com/java/pdfbox-how-to-read-pdf-file-in-java/
                /*
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();
                String pdfFileInText = tStripper.getText(document);
                String lines[] = pdfFileInText.split("\\r?\\n");
                for (String line : lines) {
                    System.out.println(line);
                }
                */

                // source: https://www.tutorialkart.com/pdfbox/extract-text-line-by-line-from-pdf/
                /*
                document = PDDocument.load(selectedFile);
                PDFTextStripper stripper = new PdfParser();
                stripper.setSortByPosition(true);
                stripper.setStartPage(0);
                stripper.setEndPage(document.getNumberOfPages());
                Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
                stripper.writeText(document, dummy);
                for (String line : lines) {
                    System.out.println(line);
                }
                */

                // source: https://www.tutorialkart.com/pdfbox/read-text-pdf-document-using-pdfbox-2-0/
                String text = new PDFTextStripper().getText(document);;
                text = text.replace("\n", " ");
                String[] textArr = text.split(" ");
                ArrayList<String> arrayList = new ArrayList<>();
                String tempStr = "";
                for(int i=0; i < textArr.length; i++) {
                    if((i%10) == 0) {
                        arrayList.add(tempStr);
                        tempStr = "";
                    } else {
                        tempStr += textArr[i];
                    }
                }
                System.out.println("Text in PDF\n---------------------------------");
                for(String temp : arrayList) {
                    if(temp.toLowerCase().contains("year")) {
                        System.out.println(temp);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
