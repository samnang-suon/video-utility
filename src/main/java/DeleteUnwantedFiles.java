import model.FileExtensionTracker;
import model.FileManager;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class DeleteUnwantedFiles {
    public static void main(String[] args) throws IOException {
        FileManager fileManager = new FileManager();
        File selectedDirectory = fileManager.openFileChooser();

        if (selectedDirectory != null) {
            // source: https://mkyong.com/swing/java-swing-how-to-make-a-confirmation-dialog/
            int input = JOptionPane.showConfirmDialog(
                    null,
                    "THIS WILL DELETED ALL (.srt, .vtt and .url) FILES INSIDE '" + selectedDirectory.getName() + "' FOLDER",
                    "WARNING",
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.ERROR_MESSAGE);

            if (input == JOptionPane.OK_OPTION) {
                File[] fileList = selectedDirectory.listFiles();

                // Captured UNWANTED files
                System.out.println("============================== UNWANTED FILES FOUND ==============================");
                for (File file : fileList) {
                    fileManager.listFilesInsideFolder(file);
                }

                // Captured ZIP files
                System.out.println("============================== ZIP FILES FOUND ==============================");
                for (File file : fileList) {
                    fileManager.listZIPFilesInsideFolder(file);
                }

                // Captured MS OFFICE files
                System.out.println("============================== MS OFFICE FILES FOUND ==============================");
                for (File file : fileList) {
                    fileManager.listMSOfficeFilesInsideFolder(file);
                }

                // Captured PROGRAMMING files
                System.out.println("============================== PROGRAMMING FILES FOUND ==============================");
                FileExtensionTracker fileExtensionTracker = new FileExtensionTracker();
                for (File file : fileList) {
                    fileManager.listProgrammingFilesInsideFolder(file, fileExtensionTracker);
                }
                fileExtensionTracker.printAll();
            } else {
                System.out.println("Operation cancelled by user.");
            }
        }
    }
}
