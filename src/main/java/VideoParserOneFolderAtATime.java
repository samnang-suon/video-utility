import model.VideoUtil;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class VideoParserOneFolderAtATime {
    public static void main(String[] args) throws IOException {
        /*
        File testVideo = new File("C:/1. GPT-3.mp4");
        long result = model.VideoUtil.getDuration(testVideo.getAbsolutePath());
        System.out.println(result + "s");
        System.out.println(model.VideoUtil.getMp4Duration(testVideo.getAbsolutePath()));
        */

        // JFileChooser
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select a course folder");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("Selected folder: " + fc.getSelectedFile());

            // source:
            // https://stackoverflow.com/questions/1844688/how-to-read-all-files-in-a-folder-from-java
            ArrayList<File> fileArrayList = new ArrayList<>();
            listFilesForFolder(fc.getSelectedFile(), fileArrayList);

            long totalSeconds = 0;
            for (int i = 0; i < fileArrayList.size(); i++) {
                // source:
                // https://stackoverflow.com/questions/3571223/how-do-i-get-the-file-extension-of-a-file-in-java
                if (FilenameUtils.getExtension(fileArrayList.get(i).getAbsolutePath()).equals("mp4")) {
                    totalSeconds += VideoUtil.getMp4Duration(fileArrayList.get(i).getAbsolutePath());
                }
            }

            System.out.println("\n\n==================== TOTAL DURATION ====================");
            formatSeconds(totalSeconds);
        }
    }

    static void listFilesForFolder(final File folder, ArrayList<File> fileArrayList) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, fileArrayList);
            } else {
                System.out.println(fileEntry.getName());
                fileArrayList.add(fileEntry);
            }
        }
    }

    static void formatSeconds(long seconds) {
        // Source:
        // https://stackoverflow.com/questions/25903354/java-convert-seconds-to-minutes-hours-and-days
        /*
        long oneHourThirtyFive = (60*60) + (60*30) + (60*5);
        long hours = TimeUnit.SECONDS.toHours(oneHourThirtyFive);
        System.out.println(hours);
        long minutes = TimeUnit.SECONDS.toMinutes(oneHourThirtyFive) - (hours*60);
        System.out.println(minutes);
        System.out.println(hours + "h" + minutes + "m");
        */
        long hours = TimeUnit.SECONDS.toHours(seconds);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (hours * 60);
        System.out.println(hours + "H" + minutes + "m");
    }
}
