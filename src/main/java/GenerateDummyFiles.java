import java.io.File;
import java.io.IOException;

public class GenerateDummyFiles {
    public static void main(String[] args) throws IOException {
        System.out.println("==================== Generating dummy files inside 'C:/TEMP' ====================");
        String[] unwantedFileExtensions = new String[] { "srt", "vtt", "url", "htm", "html", "zip", "doc", "docx", "xls", "xlsx" ,"ppt", "pptx"};
        for(String fileExtension : unwantedFileExtensions) {
            File tempFile = new File("C:/TEMP/" + generateRandomFilename() + "." + fileExtension);
            tempFile.createNewFile();
        }
    }

    static String generateRandomFilename() {
        // source: https://www.educative.io/edpresso/how-to-generate-random-numbers-in-java
        int min = 65;
        int max = 90;
        String tempFilename = "";
        for(int i=0; i < 5; i++) {
            int random_int = (int) (Math.random() * (max - min + 1) + min);
            tempFilename += (char) random_int;
        }

        return tempFilename;
    }
}
