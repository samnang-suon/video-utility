import javax.swing.*;
import java.io.File;

public class PluralsightFolderPrefixer {
    public static void main(String[] args) {
        // Aask user for folder to scan
        File tempFile = null;
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select a directory to scan");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            tempFile = fc.getSelectedFile();
            System.out.println("Opening folder: " + tempFile.getAbsolutePath());
        } else {
            System.out.println("Open command cancelled by user.");
        }

        // list selected folder content
        System.out.println("\n\n");
        File[] folderContent = tempFile.listFiles();
        for(int i=0; i < folderContent.length; i++) {
            if(folderContent[i].isDirectory()) {
                System.out.println("getName(): " + folderContent[i].getName());
                System.out.println("getAbsolutePath(): " + folderContent[i].getAbsolutePath());
                System.out.println("getPath(): " + folderContent[i].getPath());
                System.out.println("getParent(): " + folderContent[i].getParent());
            }
        }

        // Prefix each folder' folder content
        System.out.println("\n\n");
        for(int i=0; i < folderContent.length; i++) {
            if(folderContent[i].isDirectory()) {
                final String PREFIX = "PLURALSIGHT_";
                String parentPath = folderContent[i].getParent();
                System.out.println("Folder");
                System.out.println("\t\t" + folderContent[i].getAbsolutePath());
                System.out.println("was renamed to");
                File newFolder = new File(parentPath + "\\" + PREFIX + folderContent[i].getName());
                folderContent[i].renameTo(newFolder);
                System.out.println("\t\t" + newFolder.getAbsolutePath());
            }
        }
    }
}
