import model.VideoUtil;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class VideoParserAllInOne {
    public static void main(String[] args) throws IOException {
        ArrayList<File> courses = new ArrayList<>();

        // JFileChooser
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select a course folder");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File selectedFolder = fc.getSelectedFile();
            System.out.println("Selected folder: " + selectedFolder.getName());

            String[] tempList = selectedFolder.list();
            for (int i = 0; i < tempList.length; i++) {
                courses.add(new File(selectedFolder.getAbsolutePath() + "\\" + tempList[i]));
            }
        }

        System.out.println("==================== LIST FOUND FILES/FOLDERS LV1 ====================");
        for (int i = 0; i < courses.size(); i++) {
            File temp = new File(String.valueOf(courses.get(i)));
            if (
                temp.isDirectory() &&
                (temp.getName().startsWith("LYNDA_") || temp.getName().startsWith("PLURALSIGHT_") || temp.getName().startsWith("PACKT") || temp.getName().startsWith("UDEMY_") || temp.getName().startsWith("CBT") || temp.getName().startsWith("OREILLY"))
            ) {
                System.out.println("\n\nFOUND: " + courses.get(i));
                String tempDuration = getFormattedDuration(courses.get(i));
                System.out.println("DURATION: " + tempDuration);

                // Rename folder by prefixing with the duration
                File newFolderName = new File(courses.get(i).getParent() + "\\" + tempDuration + "_" + courses.get(i).getName());
                System.out.println("NEW FOLDER NAME: " + newFolderName.getName());

                // For some reasons, the code below does not work...
                // courses.get(i).renameTo(newFolderName);
                // System.out.println("RENAMED TO: " + newFolderName.getAbsolutePath());
            }
        }
    }

    static String getFormattedDuration(File folderToScan) throws IOException {
        // source:
        // https://stackoverflow.com/questions/1844688/how-to-read-all-files-in-a-folder-from-java
        ArrayList<File> fileArrayList = new ArrayList<>();
        listFilesForFolder(folderToScan, fileArrayList);

        long totalSeconds = 0;
        for (int i = 0; i < fileArrayList.size(); i++) {
            // source:
            // https://stackoverflow.com/questions/3571223/how-do-i-get-the-file-extension-of-a-file-in-java
            if (FilenameUtils.getExtension(fileArrayList.get(i).getAbsolutePath()).equals("mp4")) {
                totalSeconds += VideoUtil.getMp4Duration(fileArrayList.get(i).getAbsolutePath());
            }
        }

        return formatSeconds(totalSeconds);
    }

    static void listFilesForFolder(final File folder, ArrayList<File> fileArrayList) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, fileArrayList);
            } else {
                // System.out.println(fileEntry.getName());
                fileArrayList.add(fileEntry);
            }
        }
    }

    static String formatSeconds(long seconds) {
        // Source:
        // https://stackoverflow.com/questions/25903354/java-convert-seconds-to-minutes-hours-and-days
        /*
        long oneHourThirtyFive = (60*60) + (60*30) + (60*5);
        long hours = TimeUnit.SECONDS.toHours(oneHourThirtyFive);
        System.out.println(hours);
        long minutes = TimeUnit.SECONDS.toMinutes(oneHourThirtyFive) - (hours*60);
        System.out.println(minutes);
        System.out.println(hours + "h" + minutes + "m");
        */
        long hours = TimeUnit.SECONDS.toHours(seconds);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (hours * 60);

        String formattedDuration = hours + "h" + minutes + "m";
        if (hours < 10) {
            if (minutes < 10) {
                formattedDuration = "0" + hours + "h0" + minutes + "m";
            } else {
                formattedDuration = "0" + hours + "h" + minutes + "m";
            }
        } else {
            if (minutes < 10) {
                formattedDuration = hours + "h0" + minutes + "m";
            }
        }

        return formattedDuration;
    }
}
