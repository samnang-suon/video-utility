import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class GenerateAnnotateFile {
    public static void main(String[] args) {
        try {
            Scanner read = new Scanner(System.in);
            System.out.println("========== What is the duration of the video/audio ==========");
            System.out.print("Enter the number of hour(s): ");
            int fileHoursEntered = read.nextInt();
            System.out.print("Enter the number of minute(s): ");
            int fileMinutesEntered = read.nextInt();
            System.out.print("Enter the step to use in minute(s): ");
            int stepEntered = read.nextInt();

            int fileTotalDurationInMinutes = fileHoursEntered * 60 + fileMinutesEntered;
            // Get current date
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd");
            LocalDate localDate = LocalDate.now();
            String currentDateStr = dtf.format(localDate);

            BufferedWriter writer = new BufferedWriter(new FileWriter(currentDateStr + ".txt"));

            int currentTotalDuration = 0;
            int currentHour = 0;
            int currentMinute = 0;
            writer.write("00H00m00s:\n");
            do {
                currentTotalDuration += stepEntered;

                currentHour = currentTotalDuration / 60;
                currentMinute = currentTotalDuration % 60;

                if (currentHour < 10 && currentMinute < 10) {
                    writer.write("0" + currentHour + "H" + "0" + currentMinute + "m00s:" + "\n");
                }
                if (currentHour < 10 && currentMinute >= 10) {
                    writer.write("0" + currentHour + "H" + currentMinute + "m00s:" + "\n");
                }
                if (currentHour >= 10 && currentMinute < 10) {
                    writer.write(currentHour + "H" + "0" + currentMinute + "m00s:" + "\n");
                }
                if (currentHour >= 10 && currentMinute >= 10) {
                    writer.write(currentHour + "H" + currentMinute + "m00s:" + "\n");
                }
            } while (currentTotalDuration <= fileTotalDurationInMinutes);
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
