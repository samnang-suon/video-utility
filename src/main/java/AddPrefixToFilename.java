import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AddPrefixToFilename {
    public static void main(String[] args) {
        File folderToRead = getFolderToReadFromUser();
        if (folderToRead != null) {
            getAllFileInFolder(folderToRead);
        } else {
            // Nothing to do
        }
    }
    private static void renameFileInSameDirectory(File p_Filename, String p_Prefix) {
        System.out.println("============================== renameFileInSameDirectory() ==============================");
        // https://mkyong.com/java/how-to-rename-file-in-java/
        Path source = Paths.get(p_Filename.getAbsolutePath());
        try{
            // rename a file in the same directory
            String newFilename = p_Prefix + p_Filename.getName();
            Files.move(source, source.resolveSibling(newFilename));
            System.out.println("File '" + p_Filename + "' was renamed to '" + newFilename + "'");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void getAllFileInFolder(File p_Folder) {
        System.out.println("============================== getAllFileInFolder() ==============================");
        // https://stackabuse.com/java-list-files-in-a-directory/
        String[] pathNames = p_Folder.list();
        for (String pathname : pathNames) {
            System.out.println(">>>>> File found '" + pathname + "'");
        }

        String filePrefixToAdd = JOptionPane.showInputDialog("Enter file prefix to add:");

        for (String pathname : pathNames) {
            renameFileInSameDirectory(new File(p_Folder + "\\" + pathname), filePrefixToAdd);
        }
    }
    private static File getFolderToReadFromUser() {
        System.out.println("============================== getFolderToReadFromUser() ==============================");
        File tempFile = null;

        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select a directory to scan");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            tempFile = fc.getSelectedFile();
            System.out.println("Selected folder: " + tempFile.getAbsolutePath());
        } else {
            System.out.println("Open command cancelled by user.");
        }

        return tempFile;
    }
}
