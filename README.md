# VideoUtility
## Description
This project contains 2 Java console apps:

1. VideoParserAllInOne.java
2. VideoParserOneFolderAtATime.java

to get the total duration of all MP4 found inside that folder.

For example, let's assume you have a folder named 'Learning Zsh'.
The content of that is as follow:
```shell
Learning Zsh/
    ├───1 - Introduction
    ├───2 - 1. Zsh Foundations
    ├───3 - 2. Using Zsh
    ├───4 - 3. Customizing Zsh
    └───5 - Conclusion
```
you can run

    'VideoParserOneFolderAtATime'

and pass it that folder path. It will return '01h44m'.
From there, you could rename your folder like:

    '01h44m_Learning Zsh'
